import os
import base64
from google.cloud import aiplatform

def upload_model_sample(
    project: str,
    location: str,
    display_name: str,
    serving_container_image_uri: str,
    artifact_uri: str,
    endpoint_display_name: str,
):

    # Decode the Base64 encoded Google credentials and write them to a file
    os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = 'google-credentials.json'
    with open('google-credentials.json', 'w') as file:
        file.write(base64.b64decode(os.environ['GOOGLE_CREDENTIALS']).decode())

    aiplatform.init(project=project, location=location)

    model = aiplatform.Model.upload(
        display_name=display_name,
        artifact_uri=artifact_uri,
        serving_container_image_uri=serving_container_image_uri,
    )

    model.wait()

    # Create an Endpoint
    endpoint = aiplatform.Endpoint.create(
        display_name=endpoint_display_name,
        project=project,
        location=location,
    )

    # Deploy the Model to the Endpoint
    endpoint.deploy(
        model=model,
        deployed_model_display_name=display_name,
        traffic_percentage=100,
        sync=True
    )

    print(model.display_name)
    print(model.resource_name)
    return model

upload_model_sample(
    project="rraquedan-3914aafb",
    location="us-central1",
    display_name="credit-card-fraud-detection",
    serving_container_image_uri="us-docker.pkg.dev/vertex-ai/prediction/sklearn-cpu.1-2:latest",
    artifact_uri="gs://gitlab-vertexai-demo/models/",
    endpoint_display_name="vertex-ai-demo-endpoint",  
)
